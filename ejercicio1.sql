-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-10-2019 a las 19:48:56
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ejercicio1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto`
--

CREATE TABLE `auto` (
  `Id` int(11) NOT NULL,
  `IdMarca` int(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Descripcion` varchar(300) NOT NULL,
  `tipoCombustible` varchar(500) NOT NULL,
  `CantidadPuertas` int(11) NOT NULL,
  `Precio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto`
--

INSERT INTO `auto` (`Id`, `IdMarca`, `Nombre`, `Descripcion`, `tipoCombustible`, `CantidadPuertas`, `Precio`) VALUES
(1, 5, 'Alpine A110', 'Alpine A110 es un deportivo biplaza, ligero y de motor central-trasero, de corte pasional y que rivaliza con coches deportivos como el Alfa Romeo 4C y el Porsche 718 Cayman, apoyándose en una plataforma de nuevo desarrollo y haciendo', 'Combustible', 2, 500000),
(2, 3, 'Audi A7 Sportback', 'El Audi A7 Sportback es una berlina con tintes de coupé, del segmento X, fabricado por Audi desde 2010. Actualmente se comercializa la segunda generación, presentada en octubre de 2017 (ver nuestro a fondo con las novedades del Audi A', 'Diesel', 4, 30),
(3, 5, 'Alpine A110', 'Alpine A110 es un deportivo biplaza, ligero y de motor central-trasero, de corte pasional y que rivaliza con coches deportivos como el Alfa Romeo 4C y el Porsche 718 Cayman, apoyándose en una plataforma de nuevo desarrollo y haciendo', 'Combustible', 2, 500000),
(4, 3, 'Audi A7 Sportback', 'El Audi A7 Sportback es una berlina con tintes de coupé, del segmento X, fabricado por Audi desde 2010. Actualmente se comercializa la segunda generación, presentada en octubre de 2017 (ver nuestro a fondo con las novedades del Audi A', 'Diesel', 4, 30000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE `marcas` (
  `IdMarca` int(11) NOT NULL,
  `Nombre` varchar(100) NOT NULL,
  `Descripcion` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`IdMarca`, `Nombre`, `Descripcion`) VALUES
(1, 'Bugatti.', 'Bugatti.'),
(2, 'Abarth.', 'Abarth.'),
(3, 'Audi', 'Audi'),
(4, 'BMW.', 'BMW.'),
(5, 'Alpine', 'Alpine');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auto`
--
ALTER TABLE `auto`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `fk_marcas` (`IdMarca`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`IdMarca`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auto`
--
ALTER TABLE `auto`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
  MODIFY `IdMarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auto`
--
ALTER TABLE `auto`
  ADD CONSTRAINT `fk_marcas` FOREIGN KEY (`IdMarca`) REFERENCES `marcas` (`IdMarca`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
